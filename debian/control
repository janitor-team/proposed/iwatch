Source: iwatch
Section: admin
Priority: optional
Maintainer: Joao Eriberto Mota Filho <eriberto@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: http://iwatch.sf.net
Vcs-Git: https://salsa.debian.org/debian/iwatch.git
Vcs-Browser: https://salsa.debian.org/debian/iwatch

Package: iwatch
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${perl:Depends},
         default-mta | mail-transport-agent,
         libevent-perl,
         liblinux-inotify2-perl,
         libmail-sendmail-perl,
         libxml-simpleobject-libxml-perl,
         lsb-base (>= 3.0-6)
Suggests: sendxmpp, yowsup-cli
Description: realtime filesystem monitoring program using inotify
 inotify (inode notify) is a Linux kernel subsystem that monitors
 events in filesystems and reports those events to applications in
 real time.
 .
 inotify can be used to monitor individual files or directories.
 When a directory is monitored, inotify will return events for the
 directory itself and for files inside the directory.
 .
 iWatch is a Perl wrap to inotify to monitor changes in specific
 directories or files, sending alarms to the system administrator
 (or other destination) in real time. It can:
 .
   - Send notifications via email about changes.
   - Take programmable actions immediately, as emitting alerts via
     XMPP (jabber) messengers, Telegram or executing a local program
     or script.
   - Act as HIDS (Host-based Intrusion Detection System) or an
     integrity checker, complementing the firewall system in
     networks and improving the security.
 .
 iWatch can run as a simple command, as well a daemon.
 .
 A good example of iWatch usage is to monitor the pages directory
 in webservers to warn, in real time, about defacements or file
 insertions. Other example is to synchronize configuration files
 between machines, when they are changed, as in DHCP servers acting
 in failover mode. You also use to synchronize files, via rsync,
 when these files are changed.
